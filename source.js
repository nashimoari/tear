let mouse_x;
let mouse_y;
let mouse_in_action = false;

let myGameArea = {
    drops_colors_arr: ['#E6E6FA', '#FFE4E1', '#87CEEB', '#40E0D0', '#006400', '#2E8B57', '#CD5C5C', '#BC8F8F', '#CD5C5C'],
    zoom: 1,
    area_size: 2000,
    drops_cnt: 1000,
    drops: [],
    gamer_size: 30,
    gamer_pos_x: 100,
    gamer_pos_y: 100,
    grid_start_x: 0,
    grid_start_y: 0,
    grid_size: 50,

    speed_steps: 5,

    canvas: null,
    ctx: null,
    center_x: null,
    center_y: null,

    gamer_drop_draw: function () {
        radius = this.gamer_size * this.zoom;
        this.ctx.beginPath();
        this.ctx.arc(this.center_x, this.center_y, radius, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = 'green';
        this.ctx.fill();
        this.ctx.lineWidth = 5 * this.zoom;
        this.ctx.strokeStyle = '#003300';
        this.ctx.stroke();
    },

    drop_draw: function (a) {
        let w = this.canvas.width;
        let h = this.canvas.height;


        let x = w - this.gamer_pos_x - this.center_x + a[0];
        x = (x - w / 2) * this.zoom + w / 2;

        let y = h - this.gamer_pos_y - this.center_y + a[1];
        y = (y - h / 2) * this.zoom + h / 2;

        let radius = 12 * this.zoom;
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = this.drops_colors_arr[a[2]]; //'blue';
        this.ctx.fill();
        this.ctx.lineWidth = 5;
        //this.ctx.strokeStyle = 'blue';
        //this.ctx.stroke();
        //this.ctx.fillText("X:"+a[0],x+5,y-5);
        //this.ctx.fillText("Y:"+a[1],x+5,y+5);
    },

    drops_init: function () {
        for (let i = 0; i < this.drops_cnt; i += 1) {
            let x = Math.floor(Math.random() * (this.area_size + 1));
            let y = Math.floor(Math.random() * (this.area_size + 1));
            let color = Math.floor(Math.random() * (this.drops_colors_arr.length + 1));
            this.drops.push([x, y, color]);
        }
        //console.log(this.drops);
    },

    drops_draw: function () {
        let w = this.canvas.width;
        let h = this.canvas.height;
        let w_virtual = w / this.zoom;
        let h_virtual = h / this.zoom;
        let x_virtual_start = this.gamer_pos_x - w_virtual / 2;
        let x_virtual_end = this.gamer_pos_x + w_virtual / 2;
        let y_virtual_start = this.gamer_pos_y - h_virtual / 2;
        let y_virtual_end = this.gamer_pos_y + h_virtual / 2;


        for (let i = 0; i < this.drops.length; i += 1) {
            if ((this.drops[i][0] > x_virtual_start) && (this.drops[i][0] < x_virtual_end) && (this.drops[i][1] > y_virtual_start) && (this.drops[i][1] < y_virtual_end)) {
                this.drop_draw(this.drops[i]);
            }
        }
    },

    drops_intersection_check: function () {
        let start_x = this.gamer_pos_x - this.gamer_size;
        let end_x = this.gamer_pos_x + this.gamer_size;
        let start_y = this.gamer_pos_y - this.gamer_size;
        let end_y = this.gamer_pos_y + this.gamer_size;
        let distance = -1;


        for (let i = 0; i < this.drops.length; i += 1) {
            if ((this.drops[i][0] > start_x) && (this.drops[i][0] < end_x) && (this.drops[i][1] > start_y) && (this.drops[i][1] < end_y)) {
                distance = Math.sqrt(Math.pow((this.gamer_pos_x - this.drops[i][0]), 2) + Math.pow((this.gamer_pos_y - this.drops[i][1]), 2));
                if (distance < (this.gamer_size)) {
                    this.drop_eat(i);
                }

            }
        }


    },


    drop_eat: function (i) {
        this.drops.splice(i, 1);
        this.gamer_size += 0.1;
        this.ZoomUpdate();
    },

    init: function () {
        this.canvas = document.getElementById("canvas");
        this.ctx = this.canvas.getContext("2d");
        this.center_x = this.canvas.width / 2;
        this.center_y = this.canvas.height / 2;

        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.center_x = this.canvas.width / 2;
        this.center_y = this.canvas.height / 2;

        //document.body.appendChild(this.canvas);

        //this.canvas.width = document.body.clientWidth;
        //this.canvas.height = document.body.clientHeight;
        this.drawGrid();
        this.interval = setInterval(updateGameArea, 20);
        this.drops_init();

    },

    move: function () {
        let shift_interval_x = Math.floor(this.center_x / this.speed_steps);
        let shift_interval_y = Math.floor(this.center_y / this.speed_steps);

        let speed_x = Math.ceil(Math.abs(this.center_x - mouse_x) / shift_interval_x);
        let speed_y = Math.ceil(Math.abs(this.center_y - mouse_y) / shift_interval_y);

        if (mouse_x < this.center_x) {
            this.move_x(speed_x);
        } else {
            this.move_x(-speed_x);
        }

        if (mouse_y < this.center_y) {
            this.move_y(speed_y);
        } else {
            this.move_y(-speed_y);
        }
    },

    move_x: function (step) {
        this.gamer_pos_x += -step;
        this.grid_start_x += step;

        if (this.grid_start_x > this.grid_size) {
            this.grid_start_x = 0;
        }
    },

    move_y: function (step) {
        this.gamer_pos_y += -step;
        this.grid_start_y += step;

        if (this.grid_start_y > this.grid_size) {
            this.grid_start_y = 0;
        }
    },

    clear: function () {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    },

    drawGrid: function () {
        this.ctx.beginPath();
        this.ctx.strokeStyle = '#BBBBBB';
        let dx = this.grid_size * this.zoom;
        let dy = this.grid_size * this.zoom;

        //var x = this.grid_start_x;
        //var y = this.grid_start_y;

        let w = this.canvas.width;
        let h = this.canvas.height;

        let w_virtual = w / this.zoom;
        let h_virtual = h / this.zoom;
        let x_start = this.gamer_pos_x - w / 2;
        //if (x_start<0) {x_start = 0;}

        let y_start = this.gamer_pos_y - h / 2;
        //if (y_start<0) {y_start = 0;}
        let x_virtual_start = this.gamer_pos_x - w_virtual / 2;
        let y_virtual_start = this.gamer_pos_y - h_virtual / 2;


        let x_offset1 = this.grid_size - (x_start - Math.floor(x_start / this.grid_size) * this.grid_size);
        let x_offset2 = (x_offset1 - w / 2) * this.zoom + w / 2;
        x_offset2 = x_offset2 - Math.floor(x_offset2 / dx) * dx;


        let y_offset = Math.abs(this.grid_size - (y_start - Math.floor(y_start / this.grid_size) * this.grid_size));
        y_offset = (y_offset - h / 2) * this.zoom + h / 2;
        y_offset = y_offset - Math.floor(y_offset / dy) * dy;

        let x = x_offset2;//Math.floor((start_x - Math.floor(start_x/dx)*dx)/2); // + x_offset;
        //console.log(x_start, x_offset1, x_offset2);

        let y = y_offset; //start_y - Math.floor(start_y/dy)*dy;// - y_offset;


        this.ctx.lineWidth = 1;

        while (y < h) {
            this.ctx.moveTo(0, y);
            this.ctx.lineTo(w, y);
            this.ctx.stroke();
            y = y + dy;
        }

        while (x < w) {
            this.ctx.moveTo(x, 0);
            this.ctx.lineTo(x, h);
            this.ctx.stroke();
            x = x + dx;
        }
    },

    ZoomUpdate: function () {
        //check size client radius
        // if radius > 20% of width or height then modify zoom
        let gap = 5;
        let w = this.canvas.width / 100 * 9;
        let h = this.canvas.height / 100 * 9;
        //r = this.gamer_size*this.zoom;
        let r = Math.floor(this.gamer_size * this.zoom);
        console.log(w,h,r);
        if (((r > w - gap) && (r < w + gap)) || ((r > h - gap) && (r < h + gap))) {
            //console.log("zoom OK");
            return;
        }

        if ((r > w) || (r > h)) {
            //console.log("zoom -");
            this.zoom -= 0.01
        } else {
            //console.log("zoom +");
            this.zoom += 0.01
        }
    }

};

function start() {
    myGameArea.init();

    window.addEventListener('keydown', function (e) {
        myGameArea.keys = (myGameArea.keys || []);
        myGameArea.keys[e.keyCode] = (e.type == "keydown");
    });

    window.addEventListener('keyup', function (e) {
        myGameArea.keys[e.keyCode] = (e.type == "keydown");
    });

}


function cnvs_getCoordinates(e) {
    mouse_x = e.clientX;
    mouse_y = e.clientY;
    mouse_in_action = true;
}

function mouseout() {
    mouse_in_action = false;
}


function info_show() {
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext("2d");
    let w = canvas.width - 100;
    let h = 50;

    ctx.font = "12px Arial";
    ctx.fillStyle = "#000000";
    ctx.fillText("gamer X:" + myGameArea.gamer_pos_x, w, h);
    h += 15;
    ctx.fillText("gamer Y:" + myGameArea.gamer_pos_y, w, h);
    h += 15;
    ctx.fillText("gamer Y:" + myGameArea.zoom, w, h);


}

function updateGameArea() {
    let x, height, gap, minHeight, maxHeight, minGap, maxGap;
    /*for (i = 0; i < myObstacles.length; i += 1) {
        if (myGamePiece.crashWith(myObstacles[i])) {
            return;
        }
    }*/

    if (myGameArea.keys && myGameArea.keys[37]) {
        myGameArea.zoom -= 0.001;
    }
    if (myGameArea.keys && myGameArea.keys[39]) {
        myGameArea.zoom += 0.001;
    }
    if (myGameArea.keys && myGameArea.keys[38]) {
        myGameArea.zoom -= 0.001;
    }
    if (myGameArea.keys && myGameArea.keys[40]) {
        myGameArea.zoom += 0.001;
    }

    myGameArea.clear();

    if (mouse_in_action) {
        myGameArea.move();
    }

    myGameArea.drawGrid();
    myGameArea.drops_intersection_check();
    myGameArea.gamer_drop_draw();
    myGameArea.drops_draw();
    info_show();
}


(function () {
    /**
     * Корректировка округления десятичных дробей.
     *
     * @param {String}  type  Тип корректировки.
     * @param {Number}  value Число.
     * @param {Integer} exp   Показатель степени (десятичный логарифм основания корректировки).
     * @returns {Number} Скорректированное значение.
     */
    function decimalAdjust(type, value, exp) {
        // Если степень не определена, либо равна нулю...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // Если значение не является числом, либо степень не является целым числом...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Сдвиг разрядов
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Обратный сдвиг
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    }

    // Десятичное округление к ближайшему
    if (!Math.round10) {
        Math.round10 = function (value, exp) {
            return decimalAdjust('round', value, exp);
        };
    }
    // Десятичное округление вниз
    if (!Math.floor10) {
        Math.floor10 = function (value, exp) {
            return decimalAdjust('floor', value, exp);
        };
    }
    // Десятичное округление вверх
    if (!Math.ceil10) {
        Math.ceil10 = function (value, exp) {
            return decimalAdjust('ceil', value, exp);
        };
    }
})();
